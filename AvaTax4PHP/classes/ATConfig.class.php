<?php

/**
 *
 * @file
 * ATConfig.class.php
 * Contains various service configuration parameters as class static variables
 */
/**
 * {@link AddressServiceSoap}
 * {@link TaxServiceSoap} read this file during initialization.
 * @author    Avalara
 * @copyright � 2004 - 2011 Avalara, Inc.  All rights reserved.
 * @package   Base
 */
class ATConfig {
  protected static $Configurations = array();
  protected __IVARS;
  /**
   * Get the ingredients.
   *
   * This function calls a static fetching method against the Ingredient class
   * and returns everything matching this recipe ID
   *
   * @return array An array of Ingredient objects
   */
  public function __construct($name, $values = NULL) {
    if ($values) {
      ATConfig::$Configurations[$name] = $values;
    }
    $this->_IVARS = ATConfig::$Configurations[$name];
  }
  public function __get($n) {
    if ($n == '_ivars') {
      return parent::__get($n);
    }
    if (isset($this->_IVARS[$n])) {
      return $this->_IVARS[$n];
    }
    elseif (isset(ATConfig::$Configurations['Default'][$n])) {
      return ATConfig::$Configurations['Default'][$n];
    }
    else {
      return NULL;
    }
  }
}
/* Specify configurations by name here.  You can specify as many as you like */
$__wsdldir = dirname(__FILE__) . "/wsdl";
/* This is the default configuration - it is used if no other configuration is specified */
new ATConfig('Default', array(
    'url'      => 'no url specified',
    'addressService' => '/Address/AddressSvc.asmx',
    'taxService' => '/Tax/TaxSvc.asmx',
    'batchService ' => '/Batch/BatchSvc.asmx',
    'avacert2Service ' => '/AvaCert2/AvaCert2Svc.asmx',
    'addressWSDL' => 'file://' . $__wsdldir . '/Address.wsdl',
    'taxWSDL' => 'file://' . $__wsdldir . '/Tax.wsdl',
    'batchWSDL' => 'file://' . $__wsdldir . '/BatchSvc.wsdl',
    'avacert2WSDL' => 'file://' . $__wsdldir . '/AvaCert2Svc.wsdl',
    'account' => '<your account number here>',
    'license' => '<your license key here>',
    'adapter' => 'avatax4php,11.2.0.0',
    'client' => 'AvalaraPHPInterface,1.0',
    'name' => 'PHPAdapter',
    'trace' => TRUE));
	// Read missing values from default.
?>